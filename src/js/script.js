(function() {

    "use strict";
    const navbarMenu = document.querySelector('.top-menu-items');
    const toggles = document.querySelectorAll(".c-hamburger");

    for (let i = toggles.length - 1; i >= 0; i--) {
        let toggle = toggles[i];
        toggleHandler(toggle);
    }

    function toggleHandler(toggle) {
        toggle.addEventListener( "click", function(e) {
            e.preventDefault();
            if(this.classList.contains("is-active") === true){ this.classList.remove("is-active");
                navbarMenu.style.display = 'none'
            }
            else{
                this.classList.add("is-active");
                navbarMenu.style.display = 'inline-block'
            }
        });
    }

})();


var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    loop: true,
    loopedSlides: 10
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    loop: true,
    loopedSlides: 10
});
galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;